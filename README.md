Page Stats
====
A simple test program to get some very basic page statistics, made with Java for TomEE and AngularJS.

## Code organization
This project is divided in two foldes:
- azeti-test – server project + ui static files
- azeti-test-web – ui project only

## Compiling
### User Interface
These files already present in server project, but if you wish, you can run then separately.

Use the following commands to install libraries:
```
cd azeti-test-web/WebContent
npm install
```

To start a development web server:
```
npm run start
```

### Server
If you just want to compile, run tests and deploy all the application as a war, use the command:
```
cd azeti-test
mvn package
```
If you want to edit some UI elements, don’t forget to copy new files to azeti-test/src/main/resources/public

To run, just copy the azeti-test/target/azeti-test-0.1.0.war to your TomEE’s webapps folder or alternatively, if you open the project on eclipse, you can run Application.java as a Java application. Open <http://localhost:8080/azeti-test-0.1.0/> on your web browser to view this application.
 