'use strict';

angular.module('myApp.viewMain', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/myapp', {
    templateUrl: 'views/view_main.html',
    controller: 'ViewMainCtrl'
  });
}])

.controller('ViewMainCtrl', ['$scope', '$http', function($scope, $http) {
	console.log($scope);
	$scope.submiturl = function(formData) {
		$scope.haveValideResponse = false;
		$scope.haveError = false;
		
		if(formData && formData.url) {
			$http({
				url: 'pagestats',
				method: "GET",
				params: {url: formData.url}
			}).then(function(response) {//callback success
				console.log(response.data);
				if(response.data) {
					if(!response.data.error) {
						$scope.haveValideResponse = true;
						$scope.htmlVersion = response.data.htmlVersion;
						$scope.title = response.data.title;
						$scope.headersOccurrences = response.data.headersOccurrences;
					} else {
						$scope.haveError = true;
						if(response.data.responseCode>0) {
							$scope.error = "(Code "+response.data.responseCode+") " + response.data.error;
						}else{
							$scope.error = response.data.error;
						}
					}
				}
	        },
	        function(response) {//callback error
	            $scope.error = "Something bad happened with our server, please try again later.";
	        });
		}
	}
	
	$scope.submiturl();
	
}]);