package com.longinus.azetitest.controllers;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.longinus.azetitest.fetchers.PageStatsFetcher;
import com.longinus.azetitest.models.PageStats;

@RestController
public class PageStatsController {
	@CrossOrigin(origins = "http://localhost:8000")
	@RequestMapping("/pagestats")
    public PageStats getPagestats(@RequestParam(value="url", defaultValue="") String url) {
        return PageStatsFetcher.getPageStats(url);
    }	
}
