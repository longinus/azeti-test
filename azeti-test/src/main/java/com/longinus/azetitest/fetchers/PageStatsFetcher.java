package com.longinus.azetitest.fetchers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.List;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.DocumentType;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.longinus.azetitest.models.PageStats;

public class PageStatsFetcher {
	
	private static String generateResponseCodeExplanation(int responseCode) {
        if(responseCode / 100 == 3) {
            return "Page moved";
        }
        if(responseCode / 100 == 4) {
            switch (responseCode) {
                case 401:
                case 403:
                    return "Access to page was not authorized";
                case 404:
                    return "Page not found";
                default:
                    return "Unidentified error ("+responseCode+")";
            }
        }
        if(responseCode / 100 == 5) {
            return "The page's server reported an internal error";
        }
        return "Unidentified error ("+responseCode+")";
    }

    public static PageStats getPageStats(String url){
        PageStats pageStats = new PageStats();
        
        if(url==null || url.isEmpty()) {
        	pageStats.setError("Provided URL must not be null or empty");
        	return pageStats;
        }

        Document doc = null;
        try {
            Connection.Response response = Jsoup.connect(url).ignoreHttpErrors(true).execute();

            int responseCode = response.statusCode();
            pageStats.setResponseCode(responseCode);

            if(responseCode / 100 == 2) {// Response code 200, 201, ..., 208 assumed as ok
                doc = response.parse();

                pageStats.setTitle(doc.title());

                String doctype = "";
                List<Node> nods = doc.childNodes();
                for (Node node : nods) {
                    if (node instanceof DocumentType) {
                        DocumentType documentType = (DocumentType)node;
                        doctype = documentType.attr("publicId");
                    }
                }
                if(doctype.length()>0) {
                    String[] doctypeParts = doctype.split("\\s+");
                    if(doctypeParts.length>=3) {
                        pageStats.setHtmlVersion(doctypeParts[1]+" "+doctypeParts[2]);
                    }else {
                        pageStats.setHtmlVersion("Undefined");
                    }
                }else {
                    pageStats.setHtmlVersion("HTML 5");
                }

                Elements hTags = doc.select("h1, h2, h3, h4, h5, h6");// H goes from 1 to 6, according https://www.w3.org/MarkUp/html3/headings.html
                for (Element element:hTags) {
                    pageStats.addHeader(element.nodeName());
                }
            }else {
                pageStats.setError(generateResponseCodeExplanation(responseCode));
            }

        } catch (IllegalArgumentException | MalformedURLException e) {
        	e.printStackTrace();
        	pageStats.setError("Provided URL appear to be invalid");
		}catch (IOException e) {
        	e.printStackTrace();
            if(UnknownHostException.class.isInstance(e)) {
                pageStats.setError("Host not reachable or does not exist");
            }else {
            	pageStats.setError("Unrecognized error: "+e.getMessage());
            }
        } catch (Exception e) {
        	e.printStackTrace();
        	pageStats.setError("Unrecognized error: "+e.getMessage());
		}
        
        return pageStats;
    }
}
