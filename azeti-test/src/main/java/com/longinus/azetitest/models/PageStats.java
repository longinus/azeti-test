package com.longinus.azetitest.models;

import java.util.HashMap;

public class PageStats {
	private int responseCode;
    private String error;
    private String htmlVersion;
    private String title;
    private HashMap<String, Integer> headersOccurrences;

    public PageStats() {
        headersOccurrences = new HashMap<String, Integer>();
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getHtmlVersion() {
        return htmlVersion;
    }

    public void setHtmlVersion(String htmlVersion) {
        this.htmlVersion = htmlVersion;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    public Integer[] getHeadersOccurrences() {// returning a int array looks like the most simplistic solution for this case
    	Integer []occurrences = new Integer[6];
    	for (int i = 0; i < 6; i++) {
			occurrences[i] = headersOccurrences.getOrDefault("h"+(i+1), 0);// ensure we just pack the h1-6 and nothing more
		}
        return occurrences;
    }

    public void addHeader(String headerProvided) {
        String header = headerProvided.toLowerCase();// lower-case to make sure h1 and H1 are treated like the same
        if(headersOccurrences.containsKey(header)) {
        	headersOccurrences.put(header, headersOccurrences.get(header)+1);
        }else {
        	headersOccurrences.put(header, 1);
		}
    }
}
